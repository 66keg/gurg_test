#include "ros/ros.h"
#include "std_msgs/String.h"
#include "sensor_msgs/PointCloud2.h"
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl_conversions/pcl_conversions.h>

// Z_MIN < z < Z_MAX の範囲のデータを取り出す
#define Z_MAX 0.05	// [m]
#define Z_MIN -0.05	// [m]


// 正規化されたintensityがnI_MIN 〜 nI_MAXの範囲のものを選ぶ
#define nI_MIN 0.4 // 0 ~ 1
#define nI_MAX 0.6 // 0 ~ 1


// intensityがI_MIN 〜 I_MAXの範囲のものを選ぶ
#define I_MIN 2600
#define I_MAX 2900



ros::Publisher gurg_mod_pub, gurg_boundary_pub, gurg_normalized_pub, gurg_selected_pub, gurg_normalized_selected_pub;

void gurgCallback(const sensor_msgs::PointCloud2::ConstPtr& msg)
{
    pcl::PointCloud<pcl::PointXYZI> cloud, cloud_mod, cloud_boundary, cloud_normalized, cloud_selected, cloud_normalized_selected;
    pcl::fromROSMsg(*msg, cloud);

    int index = 0;
    double y_min = 10000;
    double y_max = -10000;

    //ROS_INFO("%d", (int)cloud.points.size());

    cloud_boundary.points.resize(2);

    for(int i = 0; i < cloud.points.size(); i++)
    {
        //ROS_INFO("%f", cloud.points[i].z);
        if(cloud.points[i].z < Z_MAX && cloud.points[i].z > Z_MIN)
        {
            cloud_mod.points.resize(index+1);
            cloud_mod.points[index] = cloud.points[i];
            index++;

            if(cloud.points[i].y < y_min)
            {
                y_min = cloud.points[i].y;
                cloud_boundary.points[0] = cloud.points[i];
            }
            if(cloud.points[i].y > y_max)
            {
                y_max = cloud.points[i].y;
                cloud_boundary.points[1] = cloud.points[i];
            }
        }
    }

    //ROS_INFO("cloud_mod.points.size() = %d", (int)cloud_mod.points.size());

    sensor_msgs::PointCloud2 gurg_mod, gurg_boundary;
    pcl::toROSMsg(cloud_mod, gurg_mod);
    gurg_mod.header = msg->header;
    gurg_mod_pub.publish(gurg_mod);

    pcl::toROSMsg(cloud_boundary, gurg_boundary);
    gurg_boundary.header = msg->header;
    gurg_boundary_pub.publish(gurg_boundary);



	for(int i = 0; i < cloud.points.size(); i++)
    {
		if(cloud.points[i].intensity > I_MIN && cloud.points[i].intensity < I_MAX)
		{
			cloud_selected.points.push_back(cloud.points[i]);
		}
	}
	sensor_msgs::PointCloud2 gurg_selected;
    pcl::toROSMsg(cloud_selected, gurg_selected);
    gurg_selected.header = msg->header;
    gurg_selected_pub.publish(gurg_selected);



	double intensity_max = 0;
	for(int i = 0; i < cloud.points.size(); i++)
    {
		if(cloud.points[i].intensity > intensity_max)
		{
			intensity_max = cloud.points[i].intensity;
		}
	}
	cloud_normalized = cloud;
	for(int i = 0; i < cloud.points.size(); i++)
    {
		cloud_normalized.points[i].intensity = cloud.points[i].intensity / intensity_max;
	}
	sensor_msgs::PointCloud2 gurg_normalized;
    pcl::toROSMsg(cloud_normalized, gurg_normalized);
    gurg_normalized_pub.publish(gurg_normalized);

	for(int i = 0; i < cloud_normalized.points.size(); i++)
    {
		if(cloud_normalized.points[i].intensity > nI_MIN && cloud_normalized.points[i].intensity < nI_MAX)
		{
			cloud_normalized_selected.points.push_back(cloud_normalized.points[i]);
		}
	}
	sensor_msgs::PointCloud2 gurg_normalized_selected;
    pcl::toROSMsg(cloud_normalized_selected, gurg_normalized_selected);
    gurg_normalized_selected.header = msg->header;
    gurg_normalized_selected_pub.publish(gurg_normalized_selected);
}


int main(int argc, char **argv)
{
  ros::init(argc, argv, "gurg_test_node");
  ros::NodeHandle n;

  ros::Subscriber sub = n.subscribe("/sensors/gurg/cloud_filtered_test", 1000, gurgCallback);
  gurg_mod_pub = n.advertise<sensor_msgs::PointCloud2>("gurg_mod", 1000);
  gurg_boundary_pub = n.advertise<sensor_msgs::PointCloud2>("gurg_boundary", 1000);
  gurg_normalized_pub = n.advertise<sensor_msgs::PointCloud2>("gurg_normalized", 1000);
  gurg_selected_pub = n.advertise<sensor_msgs::PointCloud2>("gurg_selected", 1000);
  gurg_normalized_selected_pub = n.advertise<sensor_msgs::PointCloud2>("gurg_normalized_selected", 1000);
  ros::Rate loop_rate(10);

  ros::spin();

  return 0;
}
