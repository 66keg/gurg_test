#include "ros/ros.h"
#include "std_msgs/String.h"
#include "sensor_msgs/PointCloud2.h"
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/sample_consensus/ransac.h>
#include <pcl/sample_consensus/sac_model_line.h>

ros::Publisher gurg_ransac_pub, gurg_boundary_pub;

void gurgCallback(const sensor_msgs::PointCloud2::ConstPtr& msg)
{
    pcl::PointCloud<pcl::PointXYZ> cloud_boundary;

    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_ransac (new pcl::PointCloud<pcl::PointXYZ>);

    pcl::fromROSMsg(*msg, *cloud);

    int index = 0;
    double y_min = 10000;
    double y_max = -10000;

    std::vector<int> inliers;

    pcl::SampleConsensusModelLine<pcl::PointXYZ>::Ptr
    model_l (new pcl::SampleConsensusModelLine<pcl::PointXYZ> (cloud));

    pcl::RandomSampleConsensus<pcl::PointXYZ> ransac (model_l);
    ransac.setDistanceThreshold (.05);
    ransac.computeModel();
    ransac.getInliers(inliers);

    pcl::copyPointCloud<pcl::PointXYZ>(*cloud, inliers, *cloud_ransac);
    
    cloud_boundary.points.resize(2);

    for(int i = 0; i < cloud_ransac->points.size(); i++)
    {
	if(cloud_ransac->points[i].y < y_min)
	{
	    y_min = cloud_ransac->points[i].y;
	    cloud_boundary.points[0] = cloud_ransac->points[i];
	}
	if(cloud_ransac->points[i].y > y_max)
	{
	    y_max = cloud_ransac->points[i].y;
	    cloud_boundary.points[1] = cloud_ransac->points[i];
	}
    }

    sensor_msgs::PointCloud2 gurg_ransac, gurg_boundary;
    pcl::toROSMsg(*cloud_ransac, gurg_ransac);
    gurg_ransac.header = msg->header;
    gurg_ransac_pub.publish(gurg_ransac);

    if(sqrt(pow(cloud_boundary.points[0].x - cloud_boundary.points[1].x, 2.0)
		+ pow(cloud_boundary.points[0].y - cloud_boundary.points[1].y, 2.0)) < 30)
    {
	pcl::toROSMsg(cloud_boundary, gurg_boundary);
	gurg_boundary.header = msg->header;
	gurg_boundary_pub.publish(gurg_boundary);
    }
}


int main(int argc, char **argv)
{
  ros::init(argc, argv, "gurg_ransac_node");
  ros::NodeHandle n;

  ros::Subscriber sub = n.subscribe("/sensors/gurg/cloud_filtered_test", 1000, gurgCallback);
  gurg_ransac_pub = n.advertise<sensor_msgs::PointCloud2>("gurg_ransac", 1000);
  gurg_boundary_pub = n.advertise<sensor_msgs::PointCloud2>("gurg_ransac_boundary", 1000);
  ros::Rate loop_rate(10);

  ros::spin();

  return 0;
}
